import os

def converter(firstArg, from_base, to_base):   
    if(from_base == "mm"):
        if(to_base == "mm"):
            data = int(firstArg)
        if(to_base == "cm"):
            data = int(firstArg) / 10            
        if(to_base == "dm"):
            data = int(firstArg) / 100
        if(to_base == "m"):
            data = int(firstArg) / 1000
        if(to_base == "km"):
            data = int(firstArg) / 1000000

    if(from_base == "cm"):
        if(to_base == "mm"):
            data = int(firstArg) * 10
        if(to_base == "cm"):
            data = int(firstArg)
        if(to_base == "dm"):
            data = int(firstArg) / 10
        if(to_base == "m"):
            data = int(firstArg) / 100
        if(to_base == "km"):
            data = int(firstArg) / 100000

    if(from_base == "dm"):
        if(to_base == "mm"):
            data = int(firstArg) * 100
        if(to_base == "cm"):
            data = int(firstArg) * 10     
        if(to_base == "dm"):
            data = int(firstArg) 
        if(to_base == "m"):
            data = int(firstArg) / 10
        if(to_base == "km"):
            data = int(firstArg) / 10000

    if(from_base == "m"):
        if(to_base == "mm"):
            data = int(firstArg) * 1000
        if(to_base == "cm"):
            data = int(firstArg) * 100        
        if(to_base == "dm"):
            data = int(firstArg) * 10
        if(to_base == "m"):
            data = int(firstArg)
        if(to_base == "km"):
            data = int(firstArg) / 1000 

    if(from_base == "km"):
        if(to_base == "mm"):
            data = int(firstArg) * 1000000
        if(to_base == "cm"):
            data = int(firstArg) * 100000
        if(to_base == "dm"):
            data = int(firstArg) * 10000
        if(to_base == "m"):
            data = int(firstArg) * 1000
        if(to_base == "km"):
            data = int(firstArg)
    return data


# def main():
#     print("/////////////////////////////////////////////////////////")
#     print("/////////////////////////WELCOME/////////////////////////")
#     print("///////////////////////////TO_base////////////////////////////")
#     print("/ Meters converter 1.0.1                                /")
#     print("/ How to_base use it?                                        /")
#     print("/ Basic instructions:                                   /")
#     print("/ First type number that you would like to_base converte     /")
#     print("/ Than type base that you converting from_base               /")
#     print("/ Than type base that you converting to_base                 /")
#     print("/ Thts it)                                              /")
#     print("/////////////////////////////////////////////////////////")
#     print("type num")
#     x = input()
#     print("type base from_base")
#     y = input()
#     print("type base to_base")
#     z = input()
#     result = converter(int(x), str(y), str(z))
#     print(result)


# main()