from converter import converter
import logging
import pytest

num = 1

def test_converter_meters():
    logging.info("Trying to converte from m to mm")
    assert converter(num, "m", "mm") == 1000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "m", "mm")))
    logging.info("Trying to converte from m to cm")
    assert converter(num, "m", "cm") == 100
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "m", "cm")))
    logging.info("Trying to converte from m to dm")
    assert converter(num, "m", "dm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "m", "dm")))
    logging.info("Trying to converte from m to km")
    assert converter(num, "m", "km") == 0.001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "m", "km")))

def test_converter_cm():
    logging.info("Trying to converte from cm to mm")
    assert converter(num, "cm", "mm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "cm", "mm")))
    logging.info("Trying to converte from cm to dm")
    assert converter(num, "cm", "dm") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "cm", "dm")))
    logging.info("Trying to converte from cm to m")
    assert converter(num, "cm", "m") == 0.01
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "cm", "m")))
    logging.info("Trying to converte from cm to km")
    assert converter(num, "cm", "km") == 0.00001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "cm", "km")))

def test_converter_dm():
    logging.info("Trying to converte from dm to mm")
    assert converter(num, "dm", "mm") == 100
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "dm", "mm")))
    logging.info("Trying to converte from dm to cm")
    assert converter(num, "dm", "cm") == 10
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "dm", "cm")))
    logging.info("Trying to converte from dm to m")
    assert converter(num, "dm", "m") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "dm", "m")))
    logging.info("Trying to converte from dm to km")
    assert converter(num, "dm", "km") == 0.0001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "dm", "km")))

def test_converter_km():
    logging.info("Trying to converte from km to mm")
    assert converter(num, "km", "mm") == 1000000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "km", "mm")))
    logging.info("Trying to converte from km to cm")
    assert converter(num, "km", "cm") == 100000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "km", "cm")))
    logging.info("Trying to converte from km to m")
    assert converter(num, "km", "m") == 1000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "km", "m")))
    logging.info("Trying to converte from km to dm")
    assert converter(num, "km", "dm") == 10000
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "km", "dm")))

def test_converter_mm():
    logging.info("Trying to converte from mm to cm")
    assert converter(num, "mm", "cm") == 0.1
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "mm", "cm")))
    logging.info("Trying to converte from mm to dm")
    assert converter(num, "mm", "dm") == 0.01
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "mm", "dm")))
    logging.info("Trying to converte from mm to m")
    assert converter(num, "mm", "m") == 0.001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "mm", "m")))
    logging.info("Trying to converte from mm to km")
    assert converter(num, "mm", "km") == 0.000001
    logging.info("OK")
    logging.info("result of convertation " + str(num) + " is " + str(converter(num, "mm", "km")))


logging.basicConfig(filename='test.txt', level=logging.INFO)
logging.info('start')
pytest.main()
logging.info('done')